package edu.bbte.crypto3;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URL;
import java.security.PublicKey;
import java.security.cert.Certificate;

public class Client {

    private URL url;
    private HttpsURLConnection connection;

    public Client(){

    }

    public void connect(String stringURL) throws IOException {
        url = new URL(stringURL);
        connection = (HttpsURLConnection) url.openConnection();
    }

    public String getCertificatesInfo() throws IOException{
        StringBuffer sb = new StringBuffer();
        if (connection != null) {
            sb.append("Response code: ").append(connection.getResponseCode()).append("\n");
            sb.append("Cipher suite: ").append(connection.getCipherSuite()).append("\n");
            Certificate[] certificates = connection.getServerCertificates();
            for (Certificate certificate: certificates) {
//                sb.append("Certificate: ").append(certificate).append("\n");
                sb.append("Certificate type: ").append(certificate.getType()).append("\n");
                PublicKey key = certificate.getPublicKey();
                sb.append("Certificate public key: ").append(key).append("\n");
                sb.append("Public key method: ").append(key.getAlgorithm()).append("\n");
                sb.append("-----------------------------------------\n");
            }
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            PrintWriter writer = new PrintWriter(new FileWriter("body"));
            String line;
            while ((line = reader.readLine())!= null) {
                writer.print(line);
            }
            reader.close();
            writer.close();
        } else {
            sb.append("Connection was not initialized yet, tyr calling connect first!");
        }
        return sb.toString();
    }
}
