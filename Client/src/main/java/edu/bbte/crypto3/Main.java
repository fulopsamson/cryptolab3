package edu.bbte.crypto3;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        Client client = new Client();
        try {
            client.connect("https://bnr.ro/Home-Mobile.aspx/");
            String info = client.getCertificatesInfo();
            System.out.println(info);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
